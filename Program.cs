﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using Referat.Logic;

namespace Referat{
    static class Program{
        [STAThread]
        static void Main(){
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            MainView view = new MainView();
            ReferatManager model = new ReferatManager();
            Presenter presenter = new Presenter(view, model);     // Создание объектов Model, View, Presenter для реализации паттерна MVP разделяющего GUI и логику. 

            Application.Run(view);

        }
    }
}
