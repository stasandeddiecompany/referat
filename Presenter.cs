﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Referat.Logic;

namespace Referat{

    class Presenter{       //Класс связывает графический интерфейс с логикой программы.

        private readonly IMainView view;
        private readonly IReferatManager model;

        private ReferatStore store;

        public Presenter(IMainView view, IReferatManager model){
            this.view = view;
            this.model = model;
            view.getPath += new EventHandler(ReadFile);
            model.done += new EventHandler(Done);
            store = new ReferatStore();
        }   //Конструктор принимающий интерфейсы Model и View и заносящий их в поля класса. 

        private void ReadFile(object sender, EventArgs e) {
           model.GetReferat(view.ReturnPath(), view.ReturnCutProcent());  
        }        //Обработчик события при нажатии кнопки submit.

        private void Done(object sender, EventArgs e)
        {               
            view.Done();
        }             //Обработчик события при окончании процесса.
    }
}
