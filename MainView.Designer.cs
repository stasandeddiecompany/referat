﻿namespace Referat
{
    partial class MainView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.openDocFile_dialog = new System.Windows.Forms.OpenFileDialog();
            this.selectFile_btn = new System.Windows.Forms.Button();
            this.buttonSubmit = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.numericPercents = new System.Windows.Forms.NumericUpDown();
            this.labelFileName = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.numericPercents)).BeginInit();
            this.SuspendLayout();
            // 
            // selectFile_btn
            // 
            this.selectFile_btn.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.selectFile_btn.Location = new System.Drawing.Point(156, 59);
            this.selectFile_btn.Name = "selectFile_btn";
            this.selectFile_btn.Size = new System.Drawing.Size(130, 29);
            this.selectFile_btn.TabIndex = 0;
            this.selectFile_btn.Text = "Select the file";
            this.selectFile_btn.UseVisualStyleBackColor = true;
            this.selectFile_btn.Click += new System.EventHandler(this.selectFile_btn_Click);
            // 
            // buttonSubmit
            // 
            this.buttonSubmit.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buttonSubmit.Location = new System.Drawing.Point(12, 112);
            this.buttonSubmit.Name = "buttonSubmit";
            this.buttonSubmit.Size = new System.Drawing.Size(274, 33);
            this.buttonSubmit.TabIndex = 2;
            this.buttonSubmit.Text = "To do referat";
            this.buttonSubmit.UseVisualStyleBackColor = true;
            this.buttonSubmit.Click += new System.EventHandler(this.submit_btn_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(94, 68);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(23, 20);
            this.label1.TabIndex = 4;
            this.label1.Text = "%";
            // 
            // numericPercents
            // 
            this.numericPercents.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.numericPercents.Increment = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.numericPercents.Location = new System.Drawing.Point(15, 66);
            this.numericPercents.Maximum = new decimal(new int[] {
            80,
            0,
            0,
            0});
            this.numericPercents.Name = "numericPercents";
            this.numericPercents.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.numericPercents.Size = new System.Drawing.Size(64, 22);
            this.numericPercents.TabIndex = 5;
            // 
            // labelFileName
            // 
            this.labelFileName.AutoSize = true;
            this.labelFileName.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelFileName.Location = new System.Drawing.Point(12, 29);
            this.labelFileName.Name = "labelFileName";
            this.labelFileName.Size = new System.Drawing.Size(0, 16);
            this.labelFileName.TabIndex = 6;
            // 
            // MainView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(304, 170);
            this.Controls.Add(this.labelFileName);
            this.Controls.Add(this.numericPercents);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.buttonSubmit);
            this.Controls.Add(this.selectFile_btn);
            this.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Name = "MainView";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Referater";
            ((System.ComponentModel.ISupportInitialize)(this.numericPercents)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.OpenFileDialog openDocFile_dialog;
        private System.Windows.Forms.Button selectFile_btn;
        private System.Windows.Forms.Button buttonSubmit;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown numericPercents;
        private System.Windows.Forms.Label labelFileName;

    }
}