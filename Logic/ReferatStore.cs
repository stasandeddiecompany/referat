﻿using System.IO;
using System.Data.SQLite;
using System.Data;
using System;
using System.Collections.Generic;

namespace Referat.Logic{

    class ReferatStore{                               //Класс реализующий паттерн Repozitory для работы с БД. В нашем случае это SQLite.       
        static readonly string conectionString;

        static ReferatStore()  {                                    
            var builder = new SQLiteConnectionStringBuilder();

            builder.DataSource = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Referat.db");
            conectionString = builder.ToString();

            Initialize();
        }                  //Конструктор в котором создается строка подключения к БД.      

        static void Initialize() {
            using (var conection = GetConection()) {
                using (var comand = conection.CreateCommand()) {
                    comand.CommandText = @"CREATE TABLE IF NOT EXISTS [Sentence] (
                                          [id] INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
                                          [body] VARCHAR (255) NOT NULL);";

                    comand.ExecuteNonQuery();
                }
            }
        }                //Статический метод инициализации.  Создает таблицу в базе, если ее нет. 

        static SQLiteConnection GetConection(){
            return new SQLiteConnection(conectionString).OpenAndReturn();
        }   //Статический метод подключения к базе данных.  

        public void AddSentences(List<string> sentences){
            using (var conection = GetConection()) {
               using (var comand = conection.CreateCommand()) {
                    foreach(string sentence in sentences)   {
                        comand.CommandText = "INSERT INTO Sentence (body) VALUES (@Body);";
                        comand.Parameters.Add(new SQLiteParameter("@Body", sentence));
                        comand.ExecuteNonQuery();
                    }
                }
            }
        }    //Метод добавления текста в базу по предложению в одну строку.

        public List<string> GetSentences() {
            var words = new List<string>();
            using (var conection = GetConection()){
                using (var comand = conection.CreateCommand()){
                    comand.CommandText = "SELECT body FROM Sentence;";
                        var reader = comand.ExecuteReader();
                        while (reader.Read()){
                            words.Add((string)reader["body"]);
                        }
                }
            }
            return words;
        }    //Метод возвращающий коллекцию предложений из базы. 

        public Dictionary<long,string> GetNecessarySentences(List<string> words){
            var sentances = new Dictionary<long,string>();
            using (var conection = GetConection()){
                using (var comand = conection.CreateCommand()){
                    foreach (string word in words){
                        comand.CommandText = "SELECT id, body FROM Sentence WHERE body LIKE @word;";
                        comand.Parameters.Add(new SQLiteParameter("@word", "%"+word+"%"));
                        var reader = comand.ExecuteReader();
                        while (reader.Read()){
                            if (!sentances.ContainsKey((long)reader["id"])) {
                                sentances.Add((long)reader["id"], (string)reader["body"]);
                            }
                        }
                        reader.Dispose();  
                    }
                }
            }
            return sentances;
        }   //Метод возвращающий коллекцию нужных предложения для построения готового реферата. 

        public void DeleteData(){
            using (var conection = GetConection()){
                using (var comand = conection.CreateCommand()){
                    comand.CommandText = "DELETE FROM Sentence WHERE id > 0;";
                    comand.ExecuteNonQuery();   
                }
            }

        }   //Метод очищающий базу по окончании работы приложения.
    }
}


    

