﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Diagnostics;
using Novacode;

namespace Referat.Logic{

    public interface IReferatManager {  //Объявление интерфейса логики(Model) для работы c ним в Presenter.
        string GetText(string path);
        List<string> GetSentences(string text);
        List<string> GetWords(string sentence);
        List<string> GetAllWords();
        Dictionary<string, int> ConsiderWords();
        void SaveFile(string referat);
        void GetReferat(string path,int percents);
        event EventHandler done;
    }

    class ReferatManager : IReferatManager{      //Класс с логикой - реферирования 
                                                 //опираясь на свойства структур данных а также на язык запросов LINQ организуем алгоритм реферирования текста
        private ReferatStore store;
        private int textCount;
        public event EventHandler done;

        public ReferatManager() {                //Конструктор создающий для удобства, объект класса работы с БД.
            store = new ReferatStore();
        }

        public string GetText(string path)                // Метод возвращающий переменную с текстом, принимающий путь у файлу.
        {
            string text = "";
            var extension =  Path.GetExtension(path);
            if(extension == ".txt"){
                  text = File.ReadAllText(path,Encoding.Default);
            }
            else if(extension==".docx"){
                using(DocX doc = DocX.Load(path)) {
                    text = doc.Text;                   
                }
            }
            return text;   
        }

        public List<string> GetSentences(string text) {      
            var mass = text.Split(new Char[] { '.', '?', '!' }, StringSplitOptions.RemoveEmptyEntries);
            var sentences = new List<string>();

            foreach(string sentence in mass){
                sentences.Add(sentence.Trim());
            }
            textCount = sentences.Count;
            return sentences;      
        } // Метод возвращающий коллекцию предложений из текста. Каждое предложение - элемент коллекции.

        public List<string> GetWords(string sentence){
            var mass = sentence.Split(new Char[] { ' ', ',', ':',';'}, StringSplitOptions.RemoveEmptyEntries);
            var words = new List<string>();

            foreach (string word in mass){
                if (word.Length > 4) {
                    words.Add(word);                    //Здесь можно задать длину ключевых слов. 
                } 
            }
            return words;  
        }  // Метод возвращающий коллекцию слов из предложения.

        public List<string> GetAllWords() {
            var words = new List<string>();
            foreach (string sentence in store.GetSentences()){
                foreach (string word in GetWords(sentence)){
                    words.Add(word);
                }
            }
            return words;
        }             // Метод возвращающий коллекцию слов из всего текста.

        public Dictionary<string, int> ConsiderWords()
        {   
            var words = GetAllWords();
            var groupWords = words.GroupBy(i => i);
            var dictionary = new Dictionary<string, int>();

            foreach (var word in groupWords) {
                dictionary.Add(word.Key, word.Count());
            }

            return dictionary;
        }  // Метод возвращающий словарь слов в качестве ключей используются слова, значение - количество повторений в тексте.

        public void SaveFile(string referat) {             
            File.WriteAllText("Реферат.doc", referat, Encoding.UTF8);
        }           // Метод сохраняющий готовые реферат. Путь можно задать здесь.

        public void GetReferat(string path, int percents)
        { 
            Task task = Task.Run(() =>{
                store.AddSentences(GetSentences(GetText(path)));
                var keyWords = new List<string>();
                int necessaryWordCount = textCount * percents / 100;
                var result = new List<string>();
                int i = 1;
                while (result.Count <= necessaryWordCount) {
                    var sortedWords = (from u in ConsiderWords()
                                       orderby u.Value descending
                                       select u.Key).Take(i++);
                    var value = from u in store.GetNecessarySentences(sortedWords.ToList())
                             orderby u.Key ascending
                             select u.Value;
                    result = value.ToList();
                }
                string referat = string.Join(".", result);
                SaveFile(referat);
                store.DeleteData();
                if (done != null){ done(this, EventArgs.Empty); }
            });
        }   // Метод связывающий все процессы в классе логики.
    }
}
