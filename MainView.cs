﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Referat{

    public interface IMainView { 
        string ReturnPath();
        event EventHandler getPath; 
        int ReturnCutProcent();
        void Done();
    }            // Объявление интерфейса с которым мы будем работать в Presenter. 

    public partial class MainView : Form, IMainView {   //Класс WinForms реализующий ранее объявленный интерфейс и наследующийся от базового класса Form.
        public event EventHandler getPath;         
        private string filePath;
        private int percents;

        public MainView(){
            InitializeComponent(); 
        }                     //Точка входа GUI.

        private void selectFile_btn_Click(object sender, EventArgs e) {  
                var dialog = new OpenFileDialog();
                var result = dialog.ShowDialog();
                if (result == DialogResult.OK){
                    filePath = dialog.FileName;
                    labelFileName.Text = dialog.SafeFileName;
                }   
        }   //Обработчик события при нажатии на кнопку выбора файла.

        private void submit_btn_Click(object sender, EventArgs e){
            if (numericPercents.Value != 0){
                string extension = Path.GetExtension(filePath);
                if (!string.IsNullOrEmpty(filePath) && extension == ".txt" || extension == ".docx") {
                    percents = (int)numericPercents.Value;
                    if (getPath != null)
                    { getPath(this, EventArgs.Empty); }
                    buttonSubmit.Enabled = false;
                    numericPercents.Enabled = false;
                    selectFile_btn.Enabled = false;
                    labelFileName.Text = "Processing...";

                }
                else { MessageBox.Show("Select the txt or docx file, please!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error); }
            }
            else { MessageBox.Show("Select the percents, please!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error); }
        }   //Обработчик события при нажатии на кнопку submit.

        public int ReturnCutProcent(){       //Метод возвращающий введенные пользователем проценты от изначального текста.
            return percents; 
        }

        public string ReturnPath() {
            return filePath;
        }       //Метод возвращающий введенные пользователем путь к текстовому файлу.

        public void Done(){                                                                          
            MessageBox.Show("Done!", "Success", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);  
            Application.Restart();
        }                // Метод вызывающийся из Presenter сигнализирующий, что выполнился процесс. 
    }
}
